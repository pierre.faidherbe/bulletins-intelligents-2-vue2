module.exports = {
  purge: ["./src/**/*.vue"],
  theme: {
    extend: {
      colors: {
        primary: "a1cdf4",
        vert: "#acd8aa",
        vertHover: "#9ac299",
        jaune: "#f7e08d",
        jauneHover: "#dec97e",
        bleu: "#a1cdf4",
        bleuHover: "#90b8db",
        rouge: "#fac8cd",
        rougeHover: "#e1b4b8",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
