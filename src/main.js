import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "tailwindcss/tailwind.css";
import vuetify from "./plugins/vuetify";
import VueMeta from "vue-meta";
import "./css/index.css";
import Toast from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

const options = {
  timeout: 2000,
  transition: "Vue-Toastification__fade",
  position: "bottom-right",
  hideProgressBar: true,
  showCloseButtonOnHover: true,
};

Vue.config.productionTip = true;
Vue.use(Toast, options);
Vue.use(VueMeta);

new Vue({
  store,
  router,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
