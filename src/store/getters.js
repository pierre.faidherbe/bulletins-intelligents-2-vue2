import _ from "lodash";
export const getters = {
  savoirsByMatiere: (state) => (matiere) => {
    return state.savoirs.filter(
      (savoir) => savoir.competence.matiere.id === matiere.id
    );
  },

  evaluationsByBulletin: (state) => (bulletin) => {
    return state.evaluations.filter(
      (evaluation) => evaluation.bulletin.id === bulletin.id
    );
  },

  nbOfElevesEvaluated: (state) => (evaluation) => {
    const eleves = state.eleves;

    const result = eleves.filter((eleve) =>
      evaluation.results?.some((result) => result.eleve.id === eleve.id)
    ).length;

    return `${result}/${eleves.length}`;
  },

  actualTypeEvaluation: (state) => {
    return state.actualClasse.typeEvaluations;
  },

  /* -------------------------------------------------------------------------- */
  /*                                  Matieres                                  */
  /* -------------------------------------------------------------------------- */
  getOneMatiere: (state) => (matiereId) => {
    return state.matieres.find((matiere) => matiere.id === matiereId);
  },
  getInfoByMatiere: (state) => (matiere) => {
    const comps = state.competences.filter(
      (comp) => comp.matiere.id === matiere.id
    ).length;

    return { nb: comps, text: "Compétences(s)" };
  },

  /* -------------------------------------------------------------------------- */
  /*                                 Competences                                */
  /* -------------------------------------------------------------------------- */
  getOneCompetence: (state) => (competenceId) => {
    return state.competences.find(
      (competence) => competence.id === competenceId
    );
  },

  getCompetencesByMatiere: (state) => (matiere) => {
    return state.competences.filter(
      (competence) => competence.matiere.id === matiere.id
    );
  },

  getCompetencesByBulletin: (state) => (bulletin) => {
    const savs = state.savoirs
      .filter((savoir) => savoir.bulletin.id === bulletin)
      .map((savoir) => savoir.competence);
    return _.uniqWith(savs, _.isEqual);
  },

  getInfoByCompetence: (state) => (competence) => {
    const savoirs = state.savoirs.filter(
      (savoir) => savoir.competence.id === competence.id
    ).length;

    return { nb: savoirs, text: "Savoir(s)" };
  },

  /* -------------------------------------------------------------------------- */
  /*                                   Savoirs                                  */
  /* -------------------------------------------------------------------------- */

  getOneSavoir: (state) => (savoirId) => {
    return state.savoirs.find((savoir) => savoir.id === savoirId);
  },
  getSavoirsByCompetence: (state) => (competence) => {
    return state.savoirs.filter(
      (savoir) => savoir.competence.id === competence.id
    );
  },
  savoirsByBulletinAndMatiere: (state) => (bulletin, matiere) => {
    return state.savoirs.filter(
      (savoir) =>
        savoir.bulletin.id === bulletin.id &&
        savoir.competence.matiere.id === matiere.id
    );
  },
  savoirsByBulletin: (state) => (bulletin) => {
    return state.savoirs.filter((savoir) => savoir.bulletin.id === bulletin.id);
  },

  /* -------------------------------------------------------------------------- */
  /*                                   Classes                                  */
  /* -------------------------------------------------------------------------- */

  getOneClasse: (state) => (classeId) => {
    return state.classes.find((classe) => classe.id === classeId);
  },

  getInfoByClasse: (state) => (classe) => {
    const elevesLength = state.eleves.filter(
      (eleve) => eleve.classe.id == classe.id
    ).length;

    return { nb: elevesLength, text: "Elève(s)" };
  },

  /* -------------------------------------------------------------------------- */
  /*                                   Eleves                                   */
  /* -------------------------------------------------------------------------- */

  getOneEleve: (state) => (eleveId) => {
    return state.eleves.find((eleve) => eleve.id === eleveId);
  },

  getInfoEvalByEleveAndCompetence: (state) => (eleve, competence) => {
    let nbAbsences = 0;
    let sum = 0;
    let moyenne = 0;
    let evaToCount = 0;

    const evas = state.evaluations.filter(
      (eva) =>
        eva.eleve.id == eleve.id && eva.savoir.competence.id == competence.id
    );
    evas.forEach((eva) => {
      if (eva.absent) nbAbsences++;
      else {
        evaToCount++;
        sum += eva.evaluation.coteToPourcentage;
      }
    });
    if (evaToCount === 0 || eleve.classe.typeEvaluations === "acquis")
      return { nbAbsences, evaToCount };
    else {
      moyenne = parseFloat((sum / evaToCount).toFixed(2));
      return { moyenne, nbAbsences, evaToCount };
    }
  },

  getInfoEvalByEleve: (state) => (eleve) => {
    let nbAbsences = 0;
    let sum = 0;
    let moyenne = 0;
    let evaToCount = 0;

    const evas = state.evaluations.filter((eva) => eva.eleve.id == eleve.id);
    evas.forEach((eva) => {
      if (eva.absent) nbAbsences++;
      else {
        evaToCount++;
        sum += eva.evaluation.coteToPourcentage;
      }
    });

    if (evaToCount === 0 || eleve.classe.typeEvaluations === "acquis")
      return { nbAbsences, evaToCount };
    else {
      moyenne = parseFloat((sum / evaToCount).toFixed(2));
      return { moyenne, nbAbsences, evaToCount };
    }
  },

  /* -------------------------------------------------------------------------- */
  /*                                  Bulletins                                 */
  /* -------------------------------------------------------------------------- */

  getOneBulletin: (state) => (bulletinId) => {
    return state.bulletins.find((bulletin) => bulletin.id === bulletinId);
  },
  getInfoByBulletinAndEleve: (state) => (bulletin, eleveId) => {
    const evas = state.evaluations.filter(
      (eva) => eva.bulletin.id === bulletin.id && eva.eleve.id === eleveId
    ).length;

    return { nb: evas, text: "Evaluations(s)" };
  },

  getInfoByBulletin: (state) => (bulletin) => {
    const savs = state.savoirs.filter((sav) => sav.bulletin.id === bulletin.id)
      .length;

    return { nb: savs, text: "Savoir(s)" };
  },

  /* -------------------------------------------------------------------------- */
  /*                                 Evaluations                                */
  /* -------------------------------------------------------------------------- */

  evaluationsByEleveAndCompetence: (state) => (eleve, competence) => {
    return state.evaluations.filter(
      (evaluation) =>
        evaluation.eleve.id === eleve.id &&
        evaluation.savoir.competence.id === competence.id
    );
  },

  evaluationsByCompetence: (state) => (competence) => {
    return state.evaluations.filter(
      (evaluation) => evaluation.savoir.competence.id === competence.id
    );
  },
  evaluationsBySavoir: (state) => (savoir) => {
    return state.evaluations.filter(
      (evaluation) => evaluation.savoir.id === savoir.id
    );
  },
};
