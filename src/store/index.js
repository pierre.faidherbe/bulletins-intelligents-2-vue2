import Vue from "vue";
import Vuex from "vuex";
import { vuexfireMutations } from "vuexfire";

Vue.use(Vuex);

import { actions } from "./actions";
import { getters } from "./getters";
import { state } from "./state";
/* import {
  classesModule,
  elevesModule,
  bulletinsModule,
  evaluationsModule,
  matieresModule,
  competencesModule,
  savoirsModule,
} from "./module"; */

/* const easyFirestore = VuexEasyFirestore(
  [
    classesModule,
    elevesModule,
    bulletinsModule,
    evaluationsModule,
    matieresModule,
    competencesModule,
    savoirsModule,
  ],
  {
    logging: true,
    FirebaseDependency: firebase,
  }
); */

// include as PLUGIN in your vuex store
// please note that "myModule" should ONLY be passed via the plugin
/* const storeData = {
  plugins: [easyFirestore],
  // ... your other store data
}; */

export const storeConfig = {
  state: state,
  mutations: { ...vuexfireMutations },
  actions: actions,
  getters: getters,
};

// initialise Vuex
const store = new Vuex.Store(storeConfig);

export default store;
