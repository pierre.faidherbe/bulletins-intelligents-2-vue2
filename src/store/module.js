// ~store/modules/myModule.js

/* -------------------------------------------------------------------------- */
/*                                   CLASSES                                  */
/* -------------------------------------------------------------------------- */

const classesModule = {
  firestorePath: "users/{userId}/classes",
  firestoreRefType: "collection",
  moduleName: "classes",
  statePropName: "data",
  namespaced: true, // automatically added
};

/* -------------------------------------------------------------------------- */
/*                                   ELEVES                                   */
/* -------------------------------------------------------------------------- */

const elevesModule = {
  firestorePath: "users/{userId}/eleves",
  firestoreRefType: "collection",
  moduleName: "eleves",
  statePropName: "data",
  namespaced: true, // automatically added
};

/* -------------------------------------------------------------------------- */
/*                                  BULLETINS                                 */
/* -------------------------------------------------------------------------- */

const bulletinsModule = {
  firestorePath: "users/{userId}/bulletins",
  firestoreRefType: "collection",
  moduleName: "bulletins",
  statePropName: "data",
  namespaced: true, // automatically added
};

/* -------------------------------------------------------------------------- */
/*                                 EVALUATIONS                                */
/* -------------------------------------------------------------------------- */

const evaluationsModule = {
  firestorePath: "users/{userId}/evaluations",
  firestoreRefType: "collection",
  moduleName: "evaluations",
  statePropName: "data",
  namespaced: true, // automatically added
  getters: {
    evaluationsByEleve: (state) => (eleveId) => {
      return Object.values(state.data).filter(
        (evaluation) => evaluation.eleve.id === eleveId
      );
    },

    getInfoEvalByEleve: (state) => (eleveId) => {
      let nbAbsences = 0;
      let sum = 0;
      let moyenne = 0;
      let evaToCount = 0;

      const evas = Object.values(state.data).filter(
        (eva) => eva.eleve.id == eleveId
      );

      evas.forEach((eva) => {
        if (eva.absent) nbAbsences++;
        else {
          evaToCount++;
          sum += eva.evaluation.coteToPourcentage;
        }
      });

      if (evaToCount === 0) return { nbAbsences, evaToCount };
      else {
        moyenne = (sum / evaToCount).toFixed(2);
        return { moyenne, nbAbsences, evaToCount };
      }
    },
  },
};

/* -------------------------------------------------------------------------- */
/*                                  MATIERES                                  */
/* -------------------------------------------------------------------------- */

const matieresModule = {
  firestorePath: "users/{userId}/matieres",
  firestoreRefType: "collection",
  moduleName: "matieres",
  statePropName: "data",
  namespaced: true, // automatically added
};

/* -------------------------------------------------------------------------- */
/*                                 COMPETENCES                                */
/* -------------------------------------------------------------------------- */

const competencesModule = {
  firestorePath: "users/{userId}/competences",
  firestoreRefType: "collection",
  moduleName: "competences",
  statePropName: "data",
  namespaced: true, // automatically added
  getters: {
    competencesByMatiere: (state) => (matiereId) => {
      return Object.values(state.data).filter(
        (comp) => comp.matiere.id == matiereId
      );
    },
  },
};

/* -------------------------------------------------------------------------- */
/*                                   SAVOIRS                                  */
/* -------------------------------------------------------------------------- */

const savoirsModule = {
  firestorePath: "users/{userId}/savoirs",
  firestoreRefType: "collection",
  moduleName: "savoirs",
  statePropName: "data",
  namespaced: true, // automatically added
  getters: {
    savoirsByCompetence: (state) => (competenceId) => {
      return Object.values(state.data).filter(
        (savoir) => savoir.competence.id == competenceId
      );
    },
    savoirsByBulletinAndMatiere: (state) => (bulletinId, matiere) => {
      return Object.values(state.data).filter(
        (savoir) =>
          savoir.bulletin.id === bulletinId &&
          savoir.competence.matiere.id === matiere.id
      );
    },
  },
};

export {
  classesModule,
  elevesModule,
  bulletinsModule,
  evaluationsModule,
  matieresModule,
  competencesModule,
  savoirsModule,
};
