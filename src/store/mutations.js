import { auth, db } from "../../firebase";
import router from "../router";

export const mutations = {
  /* -------------------------------------------------------------------------- */
  /*                                   classes                                  */
  /* -------------------------------------------------------------------------- */

  bindClasses(state) {
    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Classes")
      .orderBy("nom")
      .onSnapshot((snaps) => {
        return (state.classes = snaps.docs.map((doc) => {
          return { ...doc.data() };
        }));
      });
  },

  async addClasse(state, classe) {
    state.classes.push(classe);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Classes")
      .add(classe)
      .then((value) => value.update({ id: value.id }));
  },

  updateClasse(state, classe) {
    const idx = state.classes.findIndex((x) => x.id === classe.id);
    state.classes.splice(idx, 1, classe);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Classes")
      .doc(classe.id)
      .update(classe);
  },

  deleteClasse(state, classe) {
    const idx = state.classes.findIndex((x) => x.id === classe.id);
    state.classes.pop(idx);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Classes")
      .doc(classe.id)
      .delete();
  },

  setClasse(state, classe) {
    state.actualClasse = classe;
  },

  /* -------------------------------------------------------------------------- */
  /*                                   eleves                                   */
  /* -------------------------------------------------------------------------- */

  bindEleves(state) {
    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Eleves")
      .where("classe.id", "==", state.actualClasse.id)
      .onSnapshot((snaps) => {
        return (state.eleves = snaps.docs.map((doc) => {
          return { ...doc.data() };
        }));
      });
  },

  async addEleve(state, eleve) {
    //set classe of eleve
    eleve.classe = state.actualClasse;

    state.eleves.push(eleve);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Eleves")
      .add(eleve)
      .then((value) => value.update({ id: value.id }));
  },

  updateEleve(state, eleve) {
    const idx = state.eleves.findIndex((x) => x.id === eleve.id);
    state.eleves.splice(idx, 1, eleve);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Eleves")
      .doc(eleve.id)
      .update(eleve);
  },

  deleteEleve(state, eleve) {
    const idx = state.eleves.findIndex((x) => x.id === eleve.id);
    state.eleves.pop(idx);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Eleves")
      .doc(eleve.id)
      .delete();
  },

  /* -------------------------------------------------------------------------- */
  /*                                  matieres                                  */
  /* -------------------------------------------------------------------------- */

  bindMatieres(state) {
    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Matieres")
      .where("classe.id", "==", state.actualClasse.id)
      .onSnapshot((snaps) => {
        state.matieres = snaps.docs.map((doc) => {
          return { ...doc.data() };
        });
      });
  },

  addMatiere(state, matiere) {
    //set classe of matiere
    matiere.classe = state.actualClasse;

    state.matieres.push(matiere);

    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Matieres")
      .add(matiere)
      .then((value) => value.update({ id: value.id }));
  },
  updateMatiere(state, matiere) {
    const idx = state.matieres.findIndex((x) => x.id === matiere.id);
    state.matieres.splice(idx, 1, matiere);

    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Matieres")
      .doc(matiere.id)
      .update(matiere);
  },

  deleteMatiere(state, matiere) {
    const idx = state.matieres.findIndex((x) => x.id === matiere.id);
    state.matieres.pop(idx);

    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Matieres")
      .doc(matiere.id)
      .delete();
  },

  /* -------------------------------------------------------------------------- */
  /*                                 competences                                */
  /* -------------------------------------------------------------------------- */

  bindCompetences(state) {
    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Competences")
      .onSnapshot((snaps) => {
        state.competences = snaps.docs.map((doc) => {
          return { ...doc.data() };
        });
      });
  },
  bindCompetencesByMatiere(state, matiereId) {
    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Competences")
      .where("matiere.id", "==", matiereId)
      .onSnapshot((snaps) => {
        state.competences = snaps.docs.map((doc) => {
          return { ...doc.data() };
        });
      });
  },

  addCompetence(state, competence) {
    state.competences.push(competence);

    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Competences")
      .add(competence)
      .then((value) => value.update({ id: value.id }));
  },

  updateCompetence(state, competence) {
    const idx = state.competences.findIndex((x) => x.id === competence.id);
    state.competences.splice(idx, 1, competence);

    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Competences")
      .doc(competence.id)
      .update(competence);
  },

  deleteCompetence(state, competence) {
    const idx = state.competences.findIndex((x) => x.id === competence.id);
    state.competences.pop(idx);

    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Competences")
      .doc(competence.id)
      .delete();
  },

  /* -------------------------------------------------------------------------- */
  /*                                   savoirs                                  */
  /* -------------------------------------------------------------------------- */

  bindSavoirs(state) {
    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Savoirs")
      .onSnapshot((snaps) => {
        state.savoirs = snaps.docs.map((doc) => {
          return { ...doc.data() };
        });
      });
  },
  bindSavoirsByCompetence(state, competence) {
    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Savoirs")
      .where("competence.id", "==", competence.id)
      .onSnapshot((snaps) => {
        state.savoirs = snaps.docs.map((doc) => {
          return { ...doc.data() };
        });
      });
  },
  bindSavoirsByMatiere(state, matiere) {
    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Savoirs")
      .where("competence.matiere.id", "==", matiere.id)
      .onSnapshot((snaps) => {
        state.savoirs = snaps.docs.map((doc) => {
          return { ...doc.data() };
        });
      });
  },

  addSavoir(state, savoir) {
    state.savoirs.push(savoir);

    db.collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Savoirs")
      .add(savoir)
      .then((value) => value.update({ id: value.id }));
  },

  updateSavoir(state, savoir) {
    const idx = state.savoirs.findIndex((x) => x.id === savoir.id);
    state.savoirs.splice(idx, 1, savoir);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Savoirs")
      .doc(savoir.id)
      .update(savoir);
  },

  deleteSavoir(state, savoir) {
    const idx = state.savoirs.findIndex((x) => x.id === savoir.id);
    state.savoirs.pop(idx);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Savoirs")
      .doc(savoir.id)
      .delete();
  },

  /* -------------------------------------------------------------------------- */
  /*                                  bulletins                                 */
  /* -------------------------------------------------------------------------- */
  bindBulletins(state) {
    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Bulletins")
      .where("classe.id", "==", state.actualClasse.id)
      .onSnapshot((snaps) => {
        return (state.bulletins = snaps.docs.map((doc) => {
          return { ...doc.data() };
        }));
      });
  },

  async addBulletin(state, bulletin) {
    //set classe of matiere
    bulletin.classe = state.actualClasse;

    state.bulletins.push(bulletin);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Bulletins")
      .add(bulletin)
      .then((value) => value.update({ id: value.id }));
  },

  updateBulletin(state, bulletin) {
    const idx = state.bulletins.findIndex((x) => x.id === bulletin.id);
    state.bulletins.splice(idx, 1, bulletin);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Bulletins")
      .doc(bulletin.id)
      .update(bulletin);
  },

  deleteBulletin(state, bulletin) {
    const idx = state.bulletins.findIndex((x) => x.id === bulletin.id);
    state.bulletins.pop(idx);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Bulletins")
      .doc(bulletin.id)
      .delete();
  },

  /* -------------------------------------------------------------------------- */
  /*                                 evaluations                                */
  /* -------------------------------------------------------------------------- */

  bindEvaluations(state, bulletin) {
    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Evaluations")
      .where("savoir.bulletin.id", "==", bulletin.id)

      .onSnapshot((snaps) => {
        return (state.evaluations = snaps.docs.map((doc) => {
          return { ...doc.data() };
        }));
      });
  },

  async addEvaluation(state, evaluation) {
    state.evaluations.push(evaluation);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Evaluations")
      .add(evaluation)
      .then((value) => value.update({ id: value.id }));
  },

  updateEvaluation(state, evaluation) {
    const idx = state.evaluations.findIndex((x) => x.id === evaluation.id);
    state.evaluations.splice(idx, 1, evaluation);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Evaluations")
      .doc(evaluation.id)
      .update(evaluation);
  },

  deleteEvaluation(state, evaluation) {
    const idx = state.evaluations.findIndex((x) => x.id === evaluation.id);
    state.evaluations.pop(idx);

    return db
      .collection("Users")
      .doc(auth.currentUser.uid)
      .collection("Evaluations")
      .doc(evaluation.id)
      .delete();
  },

  /* -------------------------------------------------------------------------- */
  /*                                    user                                    */
  /* -------------------------------------------------------------------------- */

  signUp(state, user) {
    auth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(async () => {
        await db
          .collection("Users")
          .doc(auth.currentUser.uid)
          .set({
            nom: user.nom,
            prenom: user.prenom,
            id: auth.currentUser.uid,
          });

        return router.push({ name: "Classes" });
      });
  },
  login(state, user) {
    auth.signInWithEmailAndPassword(user.email, user.password).then(() => {
      router.push({ name: "Classes" });
    });
  },
};
