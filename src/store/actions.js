import Vue from "vue";
import { firestoreAction } from "vuexfire";
import { auth, db } from "../../firebase";
import router from "../router";

export const actions = {
  /* -------------------------------------------------------------------------- */
  /*                                   classes                                  */
  /* -------------------------------------------------------------------------- */

  bindClasses: firestoreAction((context) => {
    return context.bindFirestoreRef(
      "classes",
      db.collection(`users/${auth.currentUser.uid}/classes`)
    );
  }),

  addClasse: firestoreAction((state, classe) => {
    return classe.id != undefined
      ? db
          .collection(`users/${auth.currentUser.uid}/classes`)
          .doc(classe.id)
          .set(classe)
      : db
          .collection(`users/${auth.currentUser.uid}/classes`)
          .add(classe)
          .then((value) => value.update({ id: value.id }));
  }),

  deleteClasse(context, classe) {
    return db
      .collection(`users/${auth.currentUser.uid}/classes`)
      .doc(classe.id)
      .delete();
  },

  /* -------------------------------------------------------------------------- */
  /*                                   eleves                                   */
  /* -------------------------------------------------------------------------- */

  bindEleves: firestoreAction((context, classe) => {
    return context.bindFirestoreRef(
      "eleves",
      db
        .collection(`users/${auth.currentUser.uid}/eleves`)
        .where("classe.id", "==", classe.id)
    );
  }),
  bindAllEleves: firestoreAction((context) => {
    return context.bindFirestoreRef(
      "eleves",
      db.collection(`users/${auth.currentUser.uid}/eleves`)
    );
  }),

  addEleve: firestoreAction((state, eleve) => {
    return eleve.id != undefined
      ? db
          .collection(`users/${auth.currentUser.uid}/eleves`)
          .doc(eleve.id)
          .set(eleve)
      : db
          .collection(`users/${auth.currentUser.uid}/eleves`)
          .add(eleve)
          .then((value) => value.update({ id: value.id }));
  }),

  deleteEleve: firestoreAction((context, eleve) => {
    db.collection(`users/${auth.currentUser.uid}/eleves`)
      .doc(eleve.id)
      .delete();
  }),

  /* -------------------------------------------------------------------------- */
  /*                                  matieres                                  */
  /* -------------------------------------------------------------------------- */
  bindMatieres: firestoreAction((context, classe) => {
    return context.bindFirestoreRef(
      "matieres",
      db
        .collection(`users/${auth.currentUser.uid}/matieres`)
        .where("classe.id", "==", classe.id)
    );
  }),
  addMatiere: firestoreAction((state, matiere) => {
    return matiere.id != undefined
      ? db
          .collection(`users/${auth.currentUser.uid}/matieres`)
          .doc(matiere.id)
          .set(matiere)
      : db
          .collection(`users/${auth.currentUser.uid}/matieres`)
          .add(matiere)
          .then((value) => value.update({ id: value.id }));
  }),
  deleteMatiere: firestoreAction((context, matiere) => {
    db.collection(`users/${auth.currentUser.uid}/matieres`)
      .doc(matiere.id)
      .delete();
  }),

  /* -------------------------------------------------------------------------- */
  /*                                 competences                                */
  /* -------------------------------------------------------------------------- */
  bindCompetences: firestoreAction((context, classe) => {
    return context.bindFirestoreRef(
      "competences",
      db
        .collection(`users/${auth.currentUser.uid}/competences`)
        .where("matiere.classe.id", "==", classe.id)
    );
  }),

  addCompetence: firestoreAction((state, competence) => {
    return competence.id != undefined
      ? db
          .collection(`users/${auth.currentUser.uid}/competences`)
          .doc(competence.id)
          .set(competence)
      : db
          .collection(`users/${auth.currentUser.uid}/competences`)
          .add(competence)
          .then((value) => value.update({ id: value.id }));
  }),
  deleteCompetence: firestoreAction((context, competence) => {
    db.collection(`users/${auth.currentUser.uid}/competences`)
      .doc(competence.id)
      .delete();
  }),

  /* -------------------------------------------------------------------------- */
  /*                                   savoirs                                  */
  /* -------------------------------------------------------------------------- */

  bindSavoirs: firestoreAction((context, classe) => {
    return context.bindFirestoreRef(
      "savoirs",
      db
        .collection(`users/${auth.currentUser.uid}/savoirs`)
        .where("bulletin.classe.id", "==", classe.id)
    );
  }),

  addSavoir: firestoreAction((state, savoir) => {
    return savoir.id != undefined
      ? db
          .collection(`users/${auth.currentUser.uid}/savoirs`)
          .doc(savoir.id)
          .set(savoir)
      : db
          .collection(`users/${auth.currentUser.uid}/savoirs`)
          .add(savoir)
          .then((value) => value.update({ id: value.id }));
  }),
  deleteSavoir: firestoreAction((context, savoir) => {
    db.collection(`users/${auth.currentUser.uid}/savoirs`)
      .doc(savoir.id)
      .delete();
  }),

  /* -------------------------------------------------------------------------- */
  /*                                  bulletins                                 */
  /* -------------------------------------------------------------------------- */
  bindBulletins: firestoreAction((context, classe) => {
    return context.bindFirestoreRef(
      "bulletins",
      db
        .collection(`users/${auth.currentUser.uid}/bulletins`)
        .where("classe.id", "==", classe.id)
    );
  }),
  addBulletin: firestoreAction((state, bulletin) => {
    return bulletin.id != undefined
      ? db
          .collection(`users/${auth.currentUser.uid}/bulletins`)
          .doc(bulletin.id)
          .set(bulletin)
      : db
          .collection(`users/${auth.currentUser.uid}/bulletins`)
          .add(bulletin)
          .then((value) => value.update({ id: value.id }));
  }),
  deleteBulletin: firestoreAction((context, bulletin) => {
    db.collection(`users/${auth.currentUser.uid}/bulletins`)
      .doc(bulletin.id)
      .delete();
  }),

  /* -------------------------------------------------------------------------- */
  /*                                 evaluations                                */
  /* -------------------------------------------------------------------------- */

  bindEvaluations: firestoreAction((context, classe) => {
    return context.bindFirestoreRef(
      "evaluations",
      db
        .collection(`users/${auth.currentUser.uid}/evaluations`)
        .where("eleve.classe.id", "==", classe.id)
    );
  }),

  addEvaluation: firestoreAction((state, evaluations) => {
    evaluations.forEach((evaluation) => {
      evaluation.id != undefined
        ? db
            .collection(`users/${auth.currentUser.uid}/evaluations`)
            .doc(evaluation.id)
            .set(evaluation)
        : db
            .collection(`users/${auth.currentUser.uid}/evaluations`)
            .add(evaluation)
            .then((value) => value.update({ id: value.id }));
    });
    Vue.$toast.success("Evaluations ajoutées !");
  }),
  deleteEvaluation: firestoreAction((context, evaluation) => {
    db.collection(`users/${auth.currentUser.uid}/evaluations`)
      .doc(evaluation.id)
      .delete();
  }),
  /* -------------------------------------------------------------------------- */
  /*                                    user                                    */
  /* -------------------------------------------------------------------------- */

  signUp: firestoreAction((state, user) => {
    return auth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(async () => {
        await db
          .collection("users")
          .doc(auth.currentUser.uid)
          .set({
            nom: user.nom,
            prenom: user.prenom,
            id: auth.currentUser.uid,
          });

        return router
          .push({ name: "Classes" })
          .then(Vue.$toast.success("Inscription réussie !"));
      })
      .catch((err) => Promise.reject(err));
  }),
  login: firestoreAction((context, user) => {
    return auth
      .signInWithEmailAndPassword(user.email, user.password)
      .then(() => {
        router
          .push({ name: "Classes" })
          .then(Vue.$toast.success("Connexion réussie !"));
      })
      .catch((err) => Promise.reject(err));
  }),
  logout() {
    auth.signOut();

    router.push("/login");
  },
};
