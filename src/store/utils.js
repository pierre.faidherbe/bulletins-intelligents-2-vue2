export function getHeaders(typeEvaluations, singleEleve) {
  let headers;
  if (typeEvaluations == "cotes") {
    headers = [
      {
        text: "Résultat",
        sortable: false,
        value: "evaluation.coteToText",
      },
      {
        text: "Pourcentage",
        value: "evaluation.coteToPourcentage",
      },
      {
        text: "Commentaire",
        value: "commentaire",
      },
      { text: "Date", value: "date" },
    ];
    if (!singleEleve)
      headers.unshift(
        {
          text: "Nom",
          value: "eleve.nom",
        },
        {
          text: "Prénom",
          value: "eleve.prenom",
        }
      );

    return headers;
  } else {
    headers = [
      {
        text: "Résultat",
        sortable: false,
        value: "evaluation",
      },
      { text: "Commentaire", value: "commentaire" },

      { text: "Date", value: "date" },
    ];
    if (!singleEleve)
      headers.unshift(
        {
          text: "Nom",
          value: "eleve.nom",
        },
        {
          text: "Prénom",
          value: "eleve.prenom",
        }
      );
  }

  return headers;
}

export const fctOpenModal = (
  title,
  crud,
  type,
  matCompSavToUpdate = {},
  addingFor = {}
) => {
  return { title, crud, type, matCompSavToUpdate, addingFor };
};
