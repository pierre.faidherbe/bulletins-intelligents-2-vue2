import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/HomePage.vue";
import ElevesPage from "../views/ElevesPage.vue";
import EvaluerPage from "../views/EvaluerPage.vue";
import MatCompSavPage from "../views/MatCompSavPage.vue";
import ClassesPage from "../views/ClassesPage.vue";
import EvaluationsPage from "../views/EvaluationsPage.vue";
import SingleElevePage from "../views/SingleElevePage.vue";
import SignUpPage from "../views/Login-SignUp/SignUpPage.vue";
import LoginPage from "../views/Login-SignUp/LoginPage.vue";
import MatiereDropdown from "../components/reusable/DropdownBulletinMatiereSavoir.vue";
import BulletinsList from "../components/reusable/BulletinList.vue";
import MatieresList from "../components/reusable/AddMatieresList.vue";
import CompetencesList from "../components/reusable/CompetencesList.vue";
import SavoirsList from "../components/reusable/SavoirsList.vue";
import EvaluationsList from "../components/reusable/EvaluationsList.vue";
import Evaluate from "../components/reusable/Evaluate.vue";
import { auth } from "../../firebase";

Vue.use(VueRouter);

const routes = [
  { path: "/", redirect: "/login" },

  { path: "/login", name: "Login", component: LoginPage },
  { path: "/signup", name: "SignUp", component: SignUpPage },
  {
    path: "/classes",
    name: "Classes",
    component: ClassesPage,
    meta: {
      requiresAuth: true,
    },
    props: { matCompSav: "classe" },
  },
  {
    path: "/classes/:classeid/home",
    name: "Home",
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/classes/:classeid/eleves",
    name: "Eleves",
    component: ElevesPage,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/classes/:classeid/eleves/:eleveid",
    component: SingleElevePage,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "",
        component: BulletinsList,
        props: { canAdd: false },
      },
      {
        path: "bulletin/:bulletinid",
        component: CompetencesList,
        props: { canAdd: false },
      },
      {
        path: "bulletin/:bulletinid/competences/:competenceid/evaluations",
        component: EvaluationsList,
        props: { singleEleve: true },
        name: "EvaluationsSingleEleve",
      },
    ],
  },
  {
    path: "/classes/:classeid/matieres",
    component: MatCompSavPage,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "",
        component: MatieresList,
        props: { title: "Mes matières", matCompSav: "matiere" },
      },
      {
        path: ":matiereid/competences",
        component: CompetencesList,
        props: {
          title: "Mes compétences en ",
          canAdd: true,
          matCompSav: "competence",
        },
      },
      {
        path: ":matiereid/competences/:competenceid/savoirs",
        component: SavoirsList,
        props: { title: "Mes savoirs en " },
      },
    ],
  },
  {
    path: "/classes/:classeid/evaluer",
    component: EvaluerPage,
    meta: {
      requiresAuth: true,
    },

    children: [
      {
        path: "",
        component: BulletinsList,
        name: "Evaluer",
        props: { canAdd: true, matCompSav: "bulletin" },
      },
      {
        path: "bulletin/:bulletinid",
        component: Evaluate,
        props: { canAdd: false, matCompSav: "bulletin" },
      },
    ],
  },
  {
    path: "/classes/:classeid/evaluations",
    component: EvaluationsPage,
    meta: {
      requiresAuth: true,
    },
    children: [
      { path: "", component: BulletinsList },
      {
        path: "bulletin/:bulletinid",
        component: MatiereDropdown,
      },
      {
        path: "bulletin/:bulletinid/savoir/:savoirid/evaluations",
        component: EvaluationsList,
        props: { singleEleve: false },
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some((x) => x.meta.requiresAuth);

  if (requiresAuth && !auth.currentUser) {
    next("login");
  } else {
    next();
  }
});

export default router;
