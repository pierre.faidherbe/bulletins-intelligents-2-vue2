import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAz7r7rmaXdkqmKVdalw3g7W3uxWBYnd7A",
  authDomain: "bulletins-intelligents-2.firebaseapp.com",
  projectId: "bulletins-intelligents-2",
  storageBucket: "bulletins-intelligents-2.appspot.com",
  messagingSenderId: "904157500847",
  appId: "1:904157500847:web:0e724ed80fa90890504b5a",
  measurementId: "G-RVYBFVFJV4",
};

firebase.default.initializeApp(firebaseConfig);

const db = firebase.default.firestore();
const auth = firebase.default.auth();

if (location.hostname === "localhost") {
  auth.useEmulator("http://localhost:9099");
  db.useEmulator("localhost", 8081);
}
export { db, auth };
