/// <reference types="cypress" />

describe("My First Test", () => {
  it("Visits the app root url", () => {
    cy.visit("http://localhost:8080/");
    cy.get(":nth-child(2) > a > .bg-white").click();
    cy.get("#addMatiere").click();
    cy.get("#nom").type("Matière 1");
    cy.get(".flex-row-reverse > :nth-child(1) > .transition").click();
    cy.get(
      ":nth-child(2) > :nth-child(1) > :nth-child(1) > .v-card > .v-card__actions > .v-btn--is-elevated"
    ).click();
    cy.get("#nom").type("Compétence 1");
    cy.get(".flex-row-reverse > :nth-child(1) > .transition").click();

    //cy.contains('h1', 'Welcome to Your Vue.js App')
  });
});
