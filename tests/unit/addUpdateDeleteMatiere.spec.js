import { mount } from "@vue/test-utils";
import MatCompSavPage from "@/views/MatCompSavPage.vue";
import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { storeConfig } from "../../src/store/index";
import { cloneDeep } from "lodash";
import Vuetify from "vuetify";
import Vue from "vue";
import { db } from "../../firebase";

Vue.config.productionTip = false;
Vue.use(Vuetify);

describe("MatCompSavPage.vue", () => {
  it("should add two matières, update the first, delete the second", async () => {
    const localVue = createLocalVue();

    localVue.use(Vuex);

    const store = new Vuex.Store(cloneDeep(storeConfig));
    await store
      .dispatch("addClasse", {
        nom: "Classe 1",
        typeEvaluations: "cotes",
      })
      .then(async () =>
        //wait insert
        {
          await new Promise((r) => setTimeout(r, 500));
          store.dispatch("fetchClasses").then(async () => {
            await new Promise((r) => setTimeout(r, 500));

            store.dispatch("setClasse", store.state.classes[0]);
          });
        }
      );

    //wait insert
    await new Promise((r) => setTimeout(r, 500));

    //erase db test
    await db
      .collection("Matieres")
      .get()
      .then((snap) => snap.forEach((doc) => doc.ref.delete()));

    expect(store.state.eleves).toHaveLength(0);

    const wrapper = mount(MatCompSavPage, {
      localVue,
      store,
      vuetify: new Vuetify(),
    });

    //ajoute Matière 1
    await wrapper.get("#addMatiere").trigger("click");
    await wrapper.get("#nom").setValue("Matière 1");
    await wrapper.get("form").trigger("submit");

    //vérifie Matière 1
    expect(store.state.matieres[0].nom).toBe("Matière 1");
    expect(store.state.matieres).toHaveLength(1);

    //clique update sur Matiere 1
    await wrapper
      .findAll(".updateMatCompSav")
      .at(0)
      .trigger("click");

    //exit
    await wrapper.setData({
      modalOpen: false,
    });

    //Vérifie que le selectMatiere est empty
    await wrapper.get("#addMatiere").trigger("click");
    expect(wrapper.get("#nom").element.value).toBe("");

    //exit
    await wrapper.setData({
      modalOpen: false,
    });

    //ajoute Matière 2
    await wrapper.get("#addMatiere").trigger("click");
    await wrapper.get("#nom").setValue("Matière 2");
    await wrapper.get("form").trigger("submit");

    //vérifie Matière 2
    expect(store.state.matieres[1].nom).toBe("Matière 2");
    expect(store.state.matieres).toHaveLength(2);

    //clique sur Matière 1
    await wrapper.findAll(".updateMatCompSav").at(0).trigger("click");

    //Change Matiere 1 en Matière 3
    await wrapper.get("#nom").setValue("Matière 3");
    await wrapper.get("form").trigger("submit");

    //Vérifie Matière 3
    expect(store.state.matieres[0].nom).toBe("Matière 3");

    expect(store.state.matieres).toHaveLength(2);

    //clique update sur Matière 2
    await wrapper
      .findAll(".updateMatCompSav")
      .at(1)
      .trigger("click");

    expect(wrapper.get("#nom").element.value).toBe("Matière 2");

    //clique sur delete et vérifie
    await wrapper.get("#deleteMatCompSav").trigger("click");
    expect(store.state.matieres[0].nom).toBe("Matière 3");
    expect(store.state.matieres).toHaveLength(1);

    /*     //clique sur Matière 3
    await wrapper
      .findAllComponents({ name: "MatCompSavCardItem" })
      .at(0)
      .trigger("click");
 */
    //vérifie le nom de la tab
    //expect(wrapper.get("#matieresTab").text()).toBe('Matière 3');

    //revient en arrière
    //await wrapper.get("#matieresTab").trigger('click');

    //vérifie le nom de la tab
    //expect(wrapper.get("#matieresTab").text()).toBe('Matières');

    await db
      .collection("Matieres")
      .get()
      .then((snap) => snap.forEach((doc) => doc.ref.delete()));
    await db
      .collection("Classes")
      .get()
      .then((snap) => snap.forEach((doc) => doc.ref.delete()));
  });
});
