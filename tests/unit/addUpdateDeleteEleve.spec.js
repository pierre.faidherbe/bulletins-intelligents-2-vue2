import { mount } from "@vue/test-utils";
import ElevesPage from "@/views/ElevesPage.vue";
import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { storeConfig } from "../../src/store/index";
import { cloneDeep } from "lodash";
import { db } from "../../firebase";
import Vuetify from "vuetify";
import Vue from "vue";

Vue.config.productionTip = false;
Vue.use(Vuetify);

const lauriane = {
  nom: "Baugnies",
  prenom: "Lauriane",
  sexe: "F",
};
const pierre = {
  nom: "Faidherbe",
  prenom: "Pierre",
  sexe: "M",
};
const jean = {
  nom: "Test",
  prenom: "Jean",
  sexe: "M",
};

describe("ElevesPage.vue", () => {
  it("should add two eleves, update the first, delete the second", async () => {
    const localVue = createLocalVue();
    localVue.use(Vuex);
    const store = new Vuex.Store(cloneDeep(storeConfig));
    await store
      .dispatch("addClasse", {
        nom: "Classe 1",
        typeEvaluations: "cotes",
      })
      .then(async () =>
        //wait insert
        {
          await new Promise((r) => setTimeout(r, 500));
          store.dispatch("fetchClasses").then(async () => {
            await new Promise((r) => setTimeout(r, 500));

            store.dispatch("setClasse", store.state.classes[0]);
          });
        }
      );

    //wait insert
    await new Promise((r) => setTimeout(r, 500));

    //erase db test
    await db
      .collection("Eleves")
      .get()
      .then((snap) => snap.forEach((doc) => doc.ref.delete()));

    expect(store.state.eleves).toHaveLength(0);

    const wrapper = mount(ElevesPage, {
      localVue,
      store,
      vuetify: new Vuetify(),
    });

    //ajoute Pierre Faidherbe
    await wrapper.get("#addEleve").trigger("click");
    await wrapper.get("#nom").setValue(pierre.nom);
    await wrapper.get("#prenom").setValue(pierre.prenom);
    await wrapper.get("#M").setChecked();
    await wrapper.get("form").trigger("submit");

    //wait response
    await new Promise((r) => setTimeout(r, 500));

    //vérifie Pierre Faidherbe
    expect(
      store.state.eleves.find(
        (eleve) =>
          eleve.nom === pierre.nom &&
          eleve.prenom === pierre.prenom &&
          eleve.sexe === pierre.sexe &&
          eleve.id !== undefined
      )
    ).toBeTruthy(); //refaire
    expect(store.state.eleves).toHaveLength(1);

    //clique sur Pierre Faidherbe
    await wrapper
      .findAll(".updateEleve")
      .at(0)
      .trigger("click");

    expect(wrapper.get("#nom").element.value).toBe("Faidherbe");

    //exit
    await wrapper.setData({
      modalOpen: false,
    });

    //Vérifie que le selectEleve est empty
    await wrapper.get("#addEleve").trigger("click");
    expect(wrapper.get("#nom").element.value).toBe("");
    expect(wrapper.get("#prenom").element.value).toBe("");
    expect(wrapper.get("#M").element.checked).toBeFalsy();
    expect(wrapper.get("#F").element.checked).toBeFalsy();

    //exit
    await wrapper.setData({
      modalOpen: false,
    });

    //ajoute Lauriane Baugnies
    await wrapper.get("#addEleve").trigger("click");
    await wrapper.get("#nom").setValue(lauriane.nom);
    await wrapper.get("#prenom").setValue(lauriane.prenom);
    await wrapper.get("#F").setChecked();
    await wrapper.get("form").trigger("submit");

    //wait response
    await new Promise((r) => setTimeout(r, 500));

    //vérifie Lauriane Baugnies
    expect(
      store.state.eleves.find(
        (eleve) =>
          eleve.nom === lauriane.nom &&
          eleve.prenom === lauriane.prenom &&
          eleve.sexe === lauriane.sexe &&
          eleve.id !== undefined
      )
    ).toBeTruthy();

    expect(store.state.eleves).toHaveLength(2);

    //clique sur Pierre Faidherbe
    await wrapper
      .findAll(".updateEleve")
      .at(1)
      .trigger("click");

    //Change Pierre Faidherbe en Jean Test
    await wrapper.get("#nom").setValue(jean.nom);
    await wrapper.get("#prenom").setValue(jean.prenom);
    await wrapper.get("#M").setChecked();
    await wrapper.get("form").trigger("submit");

    //wait response
    await new Promise((r) => setTimeout(r, 500));

    //Vérifie Jean Test
    expect(
      store.state.eleves.find(
        (eleve) =>
          eleve.nom === jean.nom &&
          eleve.prenom === jean.prenom &&
          eleve.sexe === jean.sexe &&
          eleve.id !== undefined
      )
    ).toBeTruthy();

    expect(store.state.eleves).toHaveLength(2);

    //clique sur Lauriane Baugnies
    await wrapper
      .findAll(".updateEleve")
      .at(0)
      .trigger("click");

    //vérifie lauriane baugnies
    expect(wrapper.get("#nom").element.value).toBe("Baugnies");
    expect(wrapper.get("#prenom").element.value).toBe("Lauriane");
    expect(wrapper.get("#M").element.checked).toBeFalsy();
    expect(wrapper.get("#F").element.checked).toBeTruthy();

    //clique sur delete et vérifie
    await wrapper.get("#deleteEleve").trigger("click");

    //wait response
    await new Promise((r) => setTimeout(r, 500));

    expect(store.state.eleves[0].nom).toBe("Test");
    expect(store.state.eleves[0].prenom).toBe("Jean");
    expect(store.state.eleves[0].sexe).toBe("M");
    expect(store.state.eleves).toHaveLength(1);

    //erase db test
    await db
      .collection("Eleves")
      .get()
      .then((snap) => snap.forEach((doc) => doc.ref.delete()));
    await db
      .collection("Classes")
      .get()
      .then((snap) => snap.forEach((doc) => doc.ref.delete()));
  });
});
